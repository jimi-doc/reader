require("shelljs/global");

var inquirer = require("inquirer");
var chalk = require("chalk");
const {
  gitee: { account, password }
} = require("../docs/.vuepress/config");
inquirer
  .prompt([
    {
      name: "message",
      message: "请输入提交记录:",
      type: "input",
      default: ""
    }
  ])
  .then(function(answers) {
    var cmd = `git add . && git commit -m '${answers.message}' && git push`;
    exec(cmd);
    console.log(chalk.green(` 主分支 `));
    exec(
      `npm run build && cd ./dist && git init && git add . && git commit -m '${answers.message}' && git push -f https://${account}:${password}@gitee.com/jimi-doc/reader.git master:gh-pages`
    );
    console.log(chalk.green(` 打包完成并推送到码云 `));
  });
