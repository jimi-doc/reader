const puppeteer = require("puppeteer");
var chalk = require("chalk");

const {
  gitee: { account, password, warehouse }
} = require("../docs/.vuepress/config");

const Delay = timer => new Promise(resolve => setTimeout(resolve, timer));

const doLogin = async () => {
  const browser = await puppeteer.launch({
    // headless: false
  });

  const page = await browser.newPage();
  await page.goto("https://gitee.com/login");
  await page.setViewport({
    width: 1920,
    height: 900
  });

  await page.focus("#user_login");
  await page.keyboard.sendCharacter(account);
  await page.focus("#user_password");
  await page.keyboard.sendCharacter(password);
  await page.click(".submit");
  await Delay(2 * 1000);
  await page.goto(warehouse);
  page.on("dialog", async dialog => {
    await dialog.accept();
    await Delay(5 * 1000);
    await browser.close();
    console.log(chalk.green(` 发布成功 `));
  });
  await page.click(".redeploy-button");
};

doLogin();
