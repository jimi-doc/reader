/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "404.html",
    "revision": "c0ea04c3ad4d52be82c9c8b3d81e377a"
  },
  {
    "url": "assets/css/0.styles.7636880c.css",
    "revision": "4b01b66062d2d901c6a24fe7af59bb10"
  },
  {
    "url": "assets/img/create.e25f75be.png",
    "revision": "e25f75befa6d34f64851c51d60013d61"
  },
  {
    "url": "assets/img/home-bg.7b267d7c.jpg",
    "revision": "7b267d7ce30257a197aeeb29f365065b"
  },
  {
    "url": "assets/js/1.89d41b3a.js",
    "revision": "cd40feee578352e989063f0b7a1d122a"
  },
  {
    "url": "assets/js/10.3226afda.js",
    "revision": "2b3ba62cb4b28dd052f9398b668c476b"
  },
  {
    "url": "assets/js/11.efb18052.js",
    "revision": "6943741c3226bc2046f622a6ebb55fc1"
  },
  {
    "url": "assets/js/12.c2e9342c.js",
    "revision": "aada652c1cfaeb29fdd73d41d8bae70e"
  },
  {
    "url": "assets/js/13.d6fabd7d.js",
    "revision": "5536fef531bfacf79c0a943e7bcde023"
  },
  {
    "url": "assets/js/14.36f644a4.js",
    "revision": "8a294a5a1e963e5fd88a7f26c5af3dd2"
  },
  {
    "url": "assets/js/15.8e48ba49.js",
    "revision": "db97fb93c4e40d7cc54269d227241c68"
  },
  {
    "url": "assets/js/3.99e51170.js",
    "revision": "04abde1b384ba985c71e05ebbfad0ac8"
  },
  {
    "url": "assets/js/4.f5c147a2.js",
    "revision": "ba69eeb43fca43fedb26524d8f48ae29"
  },
  {
    "url": "assets/js/5.7bb20598.js",
    "revision": "4b0e252cdfe90601f33e1c9fdb738aba"
  },
  {
    "url": "assets/js/6.929de10d.js",
    "revision": "86320faf37c07cf104338407e2c254e6"
  },
  {
    "url": "assets/js/7.d880bbd6.js",
    "revision": "e6f678e79dfbb84ef7b9f2ce52f0ee60"
  },
  {
    "url": "assets/js/8.a4df3a42.js",
    "revision": "3ab3312e376e59bbe0779d07bc9f08a8"
  },
  {
    "url": "assets/js/9.1e5535ff.js",
    "revision": "53a9f2070c67110e3d3c40c1cf472912"
  },
  {
    "url": "assets/js/app.287533fa.js",
    "revision": "427110b9370c5648ca534d5706260650"
  },
  {
    "url": "avatar.png",
    "revision": "89efddd25bee8d6c8509d38f15c61d90"
  },
  {
    "url": "categories/index.html",
    "revision": "4e8e38a4dd79172c850556da2295ed0b"
  },
  {
    "url": "demo.html",
    "revision": "68f232f46432ab8601368920979e91cc"
  },
  {
    "url": "hero.png",
    "revision": "5367b9349d4e048235eeed50d9ef36df"
  },
  {
    "url": "index.html",
    "revision": "5377402effff2dd27bc94086f17b7030"
  },
  {
    "url": "logo.png",
    "revision": "406370f8f120332c7a41611803a290b6"
  },
  {
    "url": "psychology/part1/article1.html",
    "revision": "26956261b08e4f0efcbda726bfe88b4f"
  },
  {
    "url": "psychology/part1/index.html",
    "revision": "a89a0fef34bf13bf678c5820ffce5d68"
  },
  {
    "url": "tag/index.html",
    "revision": "5f8ba57669138f00d0726dc3c65288ac"
  },
  {
    "url": "time.jpg",
    "revision": "217a6e2c0d56f4b97415f4a2bf79f92f"
  },
  {
    "url": "timeline/index.html",
    "revision": "d8267c5aa7f3321bf7022653981731b0"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
addEventListener('message', event => {
  const replyPort = event.ports[0]
  const message = event.data
  if (replyPort && message && message.type === 'skip-waiting') {
    event.waitUntil(
      self.skipWaiting().then(
        () => replyPort.postMessage({ error: null }),
        error => replyPort.postMessage({ error })
      )
    )
  }
})
